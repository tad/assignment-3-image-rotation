#ifndef ERROR_LIST
#define ARGUMENT_ERROR "Number of arguments is not match\n"

#define READ_HEADER_ERROR "Cannot read header\n"
#define WRITE_HEADER_ERROR "Cannot write header\n"
#define READ_IMAGE_ERROR "Cannot read image\n"
#define WRITE_IMAGE_ERROR "Cannot write image\n"
#define ROTATE_IMAGE_ERROR "Cannot rotate image\n"
#define OPEN_FILE_ERROR "Cannot open file\n"
#define CLOSE_FILE_ERROR "Cannot close file \n"
#define SIGNATURE_ERROR "Signature is invalid"

#define WRITER_ERROR "Cannot write file\n"
#define ALL_OK "Все хорошо\n"
#endif

