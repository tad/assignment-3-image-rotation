#include "../include/utils.h"
#include "../include/errors_list.h"


void print_err(char msg[]) {
    if(!msg) {
        fprintf(stderr,"%s \n",  msg);
    }
}

int check_arguments(int num_of_argument) {
    if(num_of_argument != 3) {
        print_err(ARGUMENT_ERROR);
        return 0;
    }

    return 1;
}

void print_read_status(int st) {
    switch (st) {
        case READ_ERROR:
            print_err(SIGNATURE_ERROR);
            break;
        case READ_INVALID_SIGNATURE:
            print_err(READ_IMAGE_ERROR);
            break;
        case READ_INVALID_BITS:
            print_err(READ_HEADER_ERROR);
            break;
        default:
            printf(ALL_OK);
    };
}

void print_write_status(int st) {
    switch (st) {
        case WRITE_ERROR:
            print_err(WRITER_ERROR);
            break;
        default:
            printf(ALL_OK);
    };
}

uint8_t missing_width(uint32_t width) {
    if(width % 4 == 0) return 0;
    return 4 - (width * 3 % 4); 
}

