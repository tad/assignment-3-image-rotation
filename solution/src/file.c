#include "../include/file.h"
#include "../include/errors_list.h"
enum read_status from_bmp( FILE* in, struct image* img ) {
    
    struct bmp_header header = {0};

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) 
        return READ_INVALID_HEADER;
    enum read_status valid_header = validate_header(header);
    if( valid_header != READ_OK) return valid_header;
    *img = create_img(header.biWidth, header.biHeight);
    if(!img->data) return READ_ERROR;
    for ( uint32_t i = 0; i < img->height; ++i ) {
        if(fread(img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_INVALID_BITS;
        }
        if(fseek( in, missing_width(img->width), SEEK_CUR )) {
            return READ_INVALID_BITS;
        } 
    }

    if(!img) return READ_ERROR;
    return READ_OK;
}

struct bmp_header create_header(uint32_t width, uint32_t height) {
return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = sizeof(struct bmp_header) + height * (PIXEL_SIZE * width + missing_width(width)),
            .bfReserved = BF_REVERSED,
            .bOffBits = B_OFF_BITS,
            .biSize = BI_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = height * (PIXEL_SIZE * width + missing_width(width)),
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT
    };
}

enum write_status to_bmp( FILE* out, struct image const* img ) {

    struct bmp_header header = create_header(img->width, img->height);
    uint32_t pixel_missed = missing_width(img->width);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (uint32_t i = 0; i < img->height; ++i) {
        if(fwrite(img->data + i * img->width,sizeof(struct pixel) * img->width,1,out) != 1) {
            return WRITE_ERROR;
        };
        uint32_t empty_pixel = 0;
        if(pixel_missed != 0) {
            if(!fwrite(&empty_pixel, pixel_missed, 1, out )) return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

enum read_status validate_header(const struct bmp_header header) {
    if(header.bfType != BF_TYPE && header.bfType != 0x4349 &&header.bfType != 0x5450)
        return READ_INVALID_SIGNATURE;

    if(header.biBitCount != BI_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    return READ_OK;

}

struct image rotate( struct image const source ) {

    struct pixel *rotated_pixel = (struct pixel *) malloc(source.width * source.height * sizeof(struct pixel));
    if(!rotated_pixel) return (struct image) {
        .height = source.width, 
        .width = source.height, 
        .data = rotated_pixel};
    for (uint32_t i = 0; i < source.height; ++i) {
        for (uint32_t j = 0; j < source.width; ++j) {
            rotated_pixel[(j * source.height) + i] = source.data[((source.height - 1 - i) * source.width) + j];
        }
    }
    return (struct image) {.height = source.width, .width = source.height, .data = rotated_pixel};
}


struct image create_img(uint32_t width, uint32_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };
}

void free_img(struct image* img) {
    if(img->data) {
        free(img->data);
        img->data = NULL;
    }

}

bool check_image(struct image img) {
    if(img.data) return true;
    return false;
}

