#ifndef UTILS_H
#define UTILS_H
#include "stat.h"
#include <stdint.h>
#include <stdio.h>
    void print_err(char msg[]);
    void print_read_status(int st);
    void print_write_status(int st);
    int check_arguments(int num_of_argument);
    uint8_t missing_width(uint32_t width);
#endif

