#ifndef FILE_H
#define FILE_H
    #include "header.h"
    #include "image.h"
    #include "utils.h"
    #include <stdbool.h>
    #include <stdlib.h>

    enum read_status from_bmp( FILE* in, struct image* img );
    enum write_status to_bmp( FILE* out, struct image const* img );
    struct bmp_header create_header(uint32_t width, uint32_t height);
    struct image rotate( struct image const source );
    void free_img(struct image* img);
    enum read_status validate_header(const struct bmp_header header);
    bool check_image(struct image img);
    struct image create_img(uint32_t width, uint32_t height);
#endif

