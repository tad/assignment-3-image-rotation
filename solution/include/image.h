#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

    struct image {
        uint64_t width, height;
        struct pixel* data;
    };


    struct __attribute__ ((packed)) pixel { uint8_t b, g, r;};
#endif

