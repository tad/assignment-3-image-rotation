#include "../include/file.h"
#include "../include/errors_list.h"
#include <stdbool.h>

int main( int argc, char* argv[] ) {
    if(check_arguments(argc) == 0) return 0;

    FILE* file = fopen(argv[1], "rb");

    if(!file) {
        fprintf(stderr, OPEN_FILE_ERROR);
        return 0;
    }

    FILE* new_file = fopen(argv[2], "wb");

    if(!new_file) {
        if(!fclose(file)) {
            fprintf(stderr, CLOSE_FILE_ERROR);
            return 0;
        }
        fprintf(stderr, OPEN_FILE_ERROR);
        return 0;
    }

    struct image image = {0};

    print_read_status(from_bmp(file, &image));
    if(!check_image(image) ) {
        fprintf(stderr, ROTATE_IMAGE_ERROR);
        return 0;
    }
    struct image new_image = rotate(image);
    if(!check_image(new_image) ) {
        fprintf(stderr, ROTATE_IMAGE_ERROR);
        return 0;
    }
 
    print_write_status(to_bmp(new_file, &new_image));

    free_img(&image);
    free_img(&new_image);

    if(fclose(file) != 0 || fclose(new_file) != 0) {
        fprintf(stderr, CLOSE_FILE_ERROR);
        return 0;
    }

    return 0;
}


